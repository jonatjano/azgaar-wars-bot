const Command = require("./Command.js")
const Player = require("../database/Player");
const funcs = require("../helpers/functions");
const config = require("../config.js")
const Country = require("../database/Country");
const emoji = require("../helpers/emoji")

const comm = {
    get: {
        homeless: new Command(
            (msg, args) => {
                Player.getsByCountry(null)
                    .then(playerList => {
                        if (playerList.length === 0) {
                            msg.reply(`There are no homeless player`)
                        } else {
                            Promise.all(playerList.map(p => guild.members.fetch(p.discordId)))
                                .then(members => members.map(m => m.nickname ?? m.user.username))
                                .then(names => msg.reply(`Homeless players are ${names.join`, `}`))
                        }
                    })
            },
            Command.perm.admins
        ),
        default: new Command(
            async (msg, args) => {
                const target = await funcs.getMember(args[0])

                if (!target) {
                    const player = new Player(msg.author.id)
                    return player.init()
                        .then(p => {
                            if (p.country !== null) {
                                msg.reply(`You're a member of ${p.country.name}`)
                            } else {
                                msg.reply(`You're a homeless ${msg.member.nickname ?? msg.author.username} ${emoji.serverSpecific.hagrid}`)
                            }
                        })
                        .catch(err => console.error(err) || msg.reply(`failed to get user data, please contact <@!439790095122300928>`))
                }

                if (msg.author.id === target.id || Command.perm.admins.condition(msg.author)) {
                    const player = new Player(target.id)
                    return player.init()
                        .then(p => {
                            msg.guild.members.fetch(p.discordId)
                                .then(member => {
                                    if (player.country !== null) {
                                        msg.reply(`${member.nickname ?? member.user.username} is a member of ${p.country.name}`)
                                    } else {
                                        msg.reply(`${member.nickname ?? member.user.username} is homeless, help him find a country with \`${config.prefix}country set <player> <country>\``)
                                    }
                                })
                                .catch(err => {
                                    console.error(err)
                                    if (player.country !== null) {
                                        msg.reply(`That user is a member of ${p.country.name}`)
                                    } else {
                                        msg.reply(`That user is homeless, help him find a country with \`${config.prefix}country set <player> <country>\``)
                                    }
                                })
                        })
                        .catch(err => console.error(err) || msg.reply(`Failed to get user data`))
                } else {
                    msg.reply(`You don't have the permission to see other players country`)
                }
            }
        )
    },
    // edit: {
    //
    // },
    set: new Command(
        async (msg, args) => {
            const target = await funcs.getMember(args[0])
            const countryName = args.slice(1).join` `

            if (countryName.length === 0) { return msg.reply("You didn't specify the country name") }

            if (! target) { return msg.reply("User is not valid") }
            const player = await Player.get(target.id)
            if (! player) { return msg.reply("Player doesn't exists") }

            let country = null
            country = await Country.getByName(countryName)
            if (country === null) {
                country = new Country(null, countryName)
                await Country.insert(country)
                    .then(
                        () => globalThis.guild.channels.create(countryName, {
                            parent: config.channels.categories.country,
                            reason: "new country"
                        })
                    )
                    .then(channel => country.setDiscordChannelId(channel.id) )
                    .then(() => msg.reply("new country created successfully") )
                    .catch(err => {
                        console.error(err)
                        msg.reply("failed to create country database entry, please contact <@!439790095122300928>")
                        throw err
                    })
            }

            if (player.country) {
                player.country.discordChannel
                    .permissionOverwrites.create(player.discordId, {VIEW_CHANNEL: null}, {reason: "new player joined country", type: 1})
                    .catch(err => console.error(err) || msg.reply(`failed to remove player permission over ${player.country.name}'s channel, please contact <@!439790095122300928>`))
            }

            country.discordChannel
                .permissionOverwrites.create(player.discordId, {VIEW_CHANNEL: true}, {reason: "new player joined country", type: 1})
                .catch(err => console.error(err) || msg.reply(`failed to give player permission over ${country.name}'s channel, please contact <@!439790095122300928>`))

            const oldCountry = player.country
            player.setCountry(country)
                .then(() => {
                    if (oldCountry) {
                        funcs.sendMessageToCountryChannelIfEmpty(oldCountry)
                    }
                    msg.reply("successfully updated player's country")
                })
                .catch(err => console.error(err) || msg.reply("Failed to change player's country, but the channels permission were changed, please contact <@!439790095122300928>"))
        },
        Command.perm.admins
    ),
    /*
    rename: new Command(
        async (msg, args) => {
            const target = await funcs.getMember(args.shift())

            if (! target) { return msg.reply("User is not valid") }
            const player = await Player.get(target.id)
            if (! player) { return msg.reply("Player doesn't exists") }

            if (player.country) {

                player.country.setName(args.join` `)
                    .then(() => msg.reply("Successfully renamed the country"))
                    .catch(() => msg.reply("Failed to rename the country"))

            }
            else {
                msg.reply("That player is not member of a country")
            }

        },
        Command.perm.admins
    ),
     */
    remove: new Command(
        async (msg, args) => {
            const target = await funcs.getMember(args[0])

            if (! target) { return msg.reply("User is not valid") }
            const player = await Player.get(target.id)
            if (! player) { return msg.reply("Player doesn't exists") }

            if (player.country) {
                player.country.discordChannel
                    .permissionOverwrites.create(player.discordId, {VIEW_CHANNEL: null}, {reason: "new player joined country", type: 1})
                    .catch(err => console.error(err) || msg.reply(`failed to remove player permission over ${player.country.name}'s channel, please contact <@!439790095122300928>`))
            }
            const oldCountry = player.country
            player.setCountry(null)
                .then(() => {
                    if (oldCountry) {
                        funcs.sendMessageToCountryChannelIfEmpty(oldCountry)
                    }
                    msg.reply(`successfully made player homeless ${msg.guild.emojis.cache.get("819656600935071801")}`)
                })
                .catch(err => console.error(err) || msg.reply("Failed to change player's country, but the channel permission were changed, please contact <@!439790095122300928>"))

        },
        Command.perm.admins
    )
}
comm.default = comm.get.default
comm.s = comm.set
comm.r = comm.remove
comm["+"] = comm.s
comm["-"] = comm.r
comm.g = comm.get
comm["."] = comm.g

module.exports = comm



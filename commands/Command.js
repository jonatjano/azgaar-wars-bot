module.exports = class Command {
	static perm = require("../helpers/permissionPresets.js")

	/**
	 * @param {function(Discord.Message, array<string>): any} call
	 * @param {function(Discord.User): boolean} permission
	 */
	constructor(call, permission = Command.perm.all) {
		this.call = call
		this.permission = permission
	}
}

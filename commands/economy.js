const Command = require("./Command.js")
const Player = require("../database/Player")
const funcs = require("../helpers/functions")
const config = require("../config.js")
const Country = require("../database/Country")
const emoji = require("../helpers/emoji")

const comm = {
    balance: new Command(
        async (msg, args) => {
            const target = await funcs.getMember(args[0])

            if (!target) {
                const player = new Player(msg.author.id)
                return player.init()
                    .then(p => {
                        if (p.country) {
                            msg.reply(`Your nation possess ${emoji.serverSpecific.gold} ${p.country.money}.`)
                        } else {
                            msg.reply("You need to be part of a country to have a balance")
                        }
                    })
                    .catch(err => console.error(err) || msg.reply(`failed to get user data, please contact <@!439790095122300928>`))
            }

            if (msg.author.id === target.id || Command.perm.admins.condition(msg.author)) {
                const player = new Player(target.id)
                return player.init()
                    .then(p => {
                        if (p.country) {
                            msg.guild.members.fetch(p.discordId)
                                .then(user => msg.reply(`${user.nickname ?? user.user.username}'s country : ${p.country.name} possess ${emoji.serverSpecific.gold} ${p.country.money}`))
                                .catch(err => console.error(err) || msg.reply(`That user country : ${p.country.name} possess ${emoji.serverSpecific.gold} ${p.country.money}`))
                        } else {
                            msg.reply("That player is homeless")
                        }
                    })
                    .catch(err => console.error(err) || msg.reply(`Failed to get user data`))
            } else {
                msg.reply(`You don't have the permission to see other players balance`)
            }
        }
    ),
    work: new Command(
        async (msg, args) => {
            const target = msg.member

            let moneyToAdd = Math.floor(Math.random() * 50) + 100

            const player = new Player(target.id)
            player.init()
                .then(p => (p.country ? p.country.setMoney(p.country.money + moneyToAdd) : msg.reply("You need to be part of a country to work"))
                    .then(() => p.country.setAction(p.country.action + 1))
                    .then(() => msg.channel.send(`Your nation experienced a great increase in agriculture and sold excess supplies for ${emoji.serverSpecific.gold} ${moneyToAdd}.`))
                    .catch(err => console.error(err) || msg.reply("failed to update database entry, please contact <@!439790095122300928>"))
                )
                .catch(err => console.error(err) || msg.reply("failed to get user data, please contact <@!439790095122300928>"))
        },
        Command.perm.hasAction
    ),
    crime: new Command(
        async (msg, args) => {
            const target = msg.member

            let moneyToAdd = Math.floor(Math.random() * 450) - 150

            const player = new Player(target.id)
            player.init()
                .then(p => (p.country ? p.country.setMoney(p.country.money + moneyToAdd) : msg.reply("You need to be part of a country to work"))
                    .then(() => p.country.setAction(p.country.action + 1))
                    .then(() => {
                        if (moneyToAdd > 0) {
                            msg.channel.send(`Your nation was able to smuggle weapons to pirates in exchange for a sum of ${emoji.serverSpecific.gold} ${moneyToAdd}.`)
                        } else {
                            msg.channel.send(`Your nation attempted to smuggle arms across the border but got caught by jonatjano. You ended up paying a fine of ${emoji.serverSpecific.gold} ${-moneyToAdd}.`)
                        }
                    })
                    .catch(err => console.error(err) || msg.reply("failed to update database entry, please contact <@!439790095122300928>"))
                )
                .catch(err => console.error(err) || msg.reply("failed to get user data, please contact <@!439790095122300928>"))
        },
        Command.perm.hasAction
    ),
    leaderboard: new Command(
        async (msg, args) => {
            const target = await funcs.getMember(args[0]) ?? msg.member

            const countries = (await Country.gets())
                .sort((a,b) => b.money - a.money)

            const countryString = countries
                .map((c,i) => `\`${(i+1).toString().padStart(2)}\` ${c.name}: ${emoji.serverSpecific.gold} ${c.money}`)
                .join`\n`

            let player
            if (msg.author.id === target.id || Command.perm.admins.condition(msg.author)) {
                player = new Player(target.id)
            } else {
                return msg.reply(`You don't have the permission to see other leaderboard position`)
            }

            return player.init()
                .then(p => {
                    if (!p.country) {throw new Error("Not really an error, forget me, can't leaderboard players country 'cause he's homeless " + target?.displayName)}
                    const pos = countries.findIndex(c => c.id === p.country.id)
                    if (pos === -1) {throw new Error("Not really an error, forget me, can't leaderboard players country 'cause he's homeless " + target?.displayName)}
                    msg.channel.send(
                        `${target.displayName}'s country rank is 
\`${(pos+1).toString().padStart(2)}\` ${p.country.name}: ${emoji.serverSpecific.gold} ${p.country.money}

${countryString}`
                    )
                })
                .catch(err => console.error(err) || msg.channel.send(countryString))



            // if (!target) {
            //     const player = new Player(msg.author.id)
            //     return player.init()
            //         .then(p => {
            //             if (p.country) {
            //                 msg.reply(`Your nation possess ${emoji.serverSpecific.gold} ${p.country.money}.`)
            //             } else {
            //                 msg.reply("You need to be part of a country to have a balance")
            //             }
            //         })
            //         .catch(err => console.error(err) || msg.reply(`failed to get user data, please contact <@!439790095122300928>`))
            // }
            //
            // if (msg.author.id === target.id || Command.perm.admins.condition(msg.author)) {
            //     const player = new Player(target.id)
            //     return player.init()
            //         .then(p => {
            //             if (p.country) {
            //                 msg.guild.members.fetch(p.discordId)
            //                     .then(user => msg.reply(`${user.nickname ?? user.user.username}'s country : ${p.country.name} possess ${emoji.serverSpecific.gold} ${p.country.money}`))
            //                     .catch(err => console.error(err) || msg.reply(`That user country : ${p.country.name} possess ${emoji.serverSpecific.gold} ${p.country.money}`))
            //             } else {
            //                 msg.reply("That player is homeless")
            //             }
            //         })
            //         .catch(err => console.error(err) || msg.reply(`Failed to get user data`))
            // } else {
            //     msg.reply(`You don't have the permission to see other players action`)
            // }
        }
    )
}
comm.default = comm.balance
comm.bal = comm.balance

comm.lb = comm.leaderboard

comm.w = comm.work
comm.c = comm.crime

module.exports = comm



const funcs = require("../helpers/functions")
const Command = require("./Command.js")
const emoji = require("../helpers/emoji")
const Player = require("../database/Player.js")
const Country = require("../database/Country.js")

const comm = {
	remove: new Command(
		async (msg, args) => {
			const target = await funcs.getMember(args[0])
			const actionToGrant = args[1] ? ~~args[1] : 1

			if (!target) {
				return msg.reply("User is not valid")
			}

			const player = new Player(target.id)
			player.init()
				.then(p => (p.country ? p.country.setAction(p.country.action - actionToGrant) : msg.reply("Player is homeless"))
					.then(() => msg.react(emoji.general.validate))
					.catch(err => console.error(err) || msg.reply("failed to update database entry, please contact <@!439790095122300928>"))
				)
				.catch(err => console.error(err) || msg.reply("failed to get user data, please contact <@!439790095122300928>"))
		},
		Command.perm.admins
	),
	add: new Command(
		async (msg, args) => {
			const target = await funcs.getMember(args[0])
			const actionToConsume = args[1] ? ~~args[1] : 1

			if (!target) {
				return msg.reply("User is not valid")
			}

			const player = new Player(target.id)
			player.init()
				.then(p => (p.country ? p.country.setAction(p.country.action + actionToConsume) : msg.reply("Player is homeless"))
					.then(() => msg.react(emoji.general.validate))
					.catch(err => console.error(err) || msg.reply("failed to update database entry, please contact <@!439790095122300928>"))
				)
				.catch(err => console.error(err) || msg.reply("failed to get user data, please contact <@!439790095122300928>"))
		},
		Command.perm.admins
	),
	set: new Command(
		async (msg, args) => {
			const target = await funcs.getMember(args[0])
			const actionToSet = args[1] ? ~~args[1] : 1

			if (!target) {
				return msg.reply("User is not valid")
			}

			const player = new Player(target.id)
			player.init()
				.then(p => (p.country ? p.country.setAction(actionToSet) : msg.reply("Player is homeless"))
					.then(() => msg.react(emoji.general.validate))
					.catch(err => console.error(err) || msg.reply("failed to update database entry, please contact <@!439790095122300928>"))
				)
				.catch(err => console.error(err) || msg.reply("failed to get user data, please contact <@!439790095122300928>"))
		},
		Command.perm.admins
	),
	default: new Command(
		async (msg, args) => {
			const target = await funcs.getMember(args[0])

			if (!target) {
				const player = new Player(msg.author.id)
				return player.init()
					.then(p => {
						if (p.country) {
							msg.reply(`Your country has done ${p.country.action} / ${p.country.maxAction} action${p.country.action > 1 ? "s" : ""}`)
						} else {
							msg.reply("You need to be part of a country to have your action counted")
						}
					})
					.catch(err => console.error(err) || msg.reply(`failed to get user data, please contact <@!439790095122300928>`))
			}

			if (msg.author.id === target.id || Command.perm.admins.condition(msg.author)) {
				const player = new Player(target.id)
				return player.init()
					.then(p => {
						if (p.country) {
							msg.guild.members.fetch(p.discordId)
								.then(user => msg.reply(`${user.nickname ?? user.user.username}'s country : ${p.country.name} has done ${p.country.action} / ${p.country.maxAction} action${p.country.action > 1 ? "s" : ""}`))
								.catch(err => console.error(err) || msg.reply(`That user country : ${p.country.name} has done ${p.country.action} / ${p.country.maxAction} action${m.action > 1 ? "s" : ""}`))
						} else {
							msg.reply("That player is homeless")
						}
					})
					.catch(err => console.error(err) || msg.reply(`Failed to get user data`))
			} else {
				msg.reply(`You don't have the permission to see other players action`)
			}
		}
	)
}
comm.consume = comm.add
comm.give = comm.remove
comm.c = comm.consume
comm.g = comm.give

module.exports = comm



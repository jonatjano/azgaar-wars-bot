const Command = require("./Command.js")
const dbManager = require("../database/dbManager.js")
const functions = require("../helpers/functions.js")
const Player = require("../database/Player.js")
const Country = require("../database/Country.js")
const config = require("../config.js")
const emoji = require("../helpers/emoji.js")

const comm = {
	db: {
		run: new Command(
			(msg, args) => {
				dbManager.sqlRun(args.join` `)
					.then(result => msg.channel.send("```\n" + JSON.stringify(result) + "\n```"))
					.catch(error => msg.channel.send("```\n" + error.stack + "\n```"))
			},
			Command.perm.creator
		),
		default: new Command(
			(msg, args) => {
				dbManager.sqlAll(args.join` `)
					.then(result => msg.channel.send("```\n" + JSON.stringify(result) + "\n```"))
					.catch(error => msg.channel.send("```\n" + error.stack + "\n```"))
			},
			Command.perm.creator
		)
	},
	exec: new Command(
		(msg, args) => {
			functions.exec(args.join` `)
				.then(result => msg.channel.send("```\n" + result + "\n```"))
				.catch(({error}) => msg.channel.send("```\n" + error.message + "\n```"))
		},
		Command.perm.creator
	),
	default: new Command(
		(msg, args) => {
			const IConfirmIWantToDeleteTheCurrentGameStateAndStartANewSeason = async () => {
				return Promise.all(
					await Country.gets()
						.then(countries => countries.map(country => country.discordChannel?.delete()))
				)
					.catch(err => msg.reply("failed to delete country channels ```\n" + err.message + "\n```"))
					.then(() => dbManager.sqlRun("DELETE FROM country")).catch(err => msg.reply("failed to clear country database ```\n" + err.message + "\n```"))
					.then(() => dbManager.sqlRun("DELETE FROM player")).catch(err => msg.reply("failed to clear player database ```\n" + err.message + "\n```"))
					.then(() => "successfully cleared everything, ready for new season :slight_smile:")
			}
			const newSeason = IConfirmIWantToDeleteTheCurrentGameStateAndStartANewSeason.name

			return new Promise(async (res, rej) => {
				let result
				try {
					result = await eval(args.join` `)
					if (typeof result !== "string") {
						result = JSON.stringify(result)
					}
				} catch (e) {
					rej(e)
				}
				res(result)
			})
				.then(result => msg.channel.send("```\n" + result + "\n```"))
				.catch(error => msg.channel.send(error.constructor.name + "```\n" + error.stack + "\n```"))
		},
		Command.perm.creator
	),
}
comm.run = comm.exec

module.exports = comm

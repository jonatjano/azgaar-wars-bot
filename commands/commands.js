const Player = require("../database/Player.js")
const Country = require("../database/Country.js")
const funcs = require("../helpers/functions.js")

const Command = require("./Command.js")
const emoji = require("../helpers/emoji.js")
const config = require("../config.js");

const comm = {
	action: require("./action.js"),
	eval: require("./eval.js"),
	country: require("./country.js"),
	economy: require("./economy.js"),

	restart: new Command(
		(msg, args) => {
			msg.react(emoji.general.validate)
				.catch(()=>{})
				.finally(process.exit(0))
		},
		Command.perm.admins
	),
	upgrade: new Command(
		(msg, args) => {
			funcs.exec("git pull")
				.then(out => console.log(out))
				.then(msg.react(emoji.general.validate))
				.catch((err, stderr) => console.error(err) || console.error(stderr))
				.finally(() => { process.exit(1) })
		},
		Command.perm.admins
	),
	register: new Command(
		async (msg, args) => {
			const target = await funcs.getMember(args[0])
			const countryName = args.slice(1).join` `

			if (!target) { return msg.reply("User is not valid") }
			if (await Player.get(target.id)) { return msg.reply("Player already registered") }

			let country = null
			if (countryName.length !== 0) {
				country = await Country.getsByName(countryName)
				if (country.length > 1) {
					return msg.reply(`Couldn't find requested country, did you mean one of : "${country.map(c => c.name).join(`", "`)}" ?`)
				}
				country = country[0]
				if (!country) {
					country = new Country(null, countryName)
					await Country.insert(country)
						.then(
							() => globalThis.guild.channels.create(countryName, {
								parent: config.channels.categories.country,
								reason: "new country",
								permissionOverwrites: [
									{ id: "809022392512020480", deny: "VIEW_CHANNEL", type: "role"}, // everyone
									{ id: "817866573560938557", allow: "VIEW_CHANNEL", type: "role"}, // owner
									{ id: "824343934876385360", allow: "VIEW_CHANNEL", type: "role"}, // guardian of peace
									{ id: "817866750066032680", allow: "VIEW_CHANNEL", type: "role"}, // administrator
									{ id: "818084087033888798", allow: "VIEW_CHANNEL", type: "role"}, // bots
									{ id: "817867045654888500", allow: "VIEW_CHANNEL", type: "role"}, // moderator
									{ id: "818080339356024833", allow: "VIEW_CHANNEL", type: "role"}, // trial moderator
									{ id: "839595066838351922", allow: "VIEW_CHANNEL", type: "role"} // season host
								],
								topic: `__**Burgs:**__

- ☆

__**Stats:**__

Stability: /10
Satisfaction: /10
Economy: /10

__**Regiments:**__

- 

__**Research:**__

(WIP)

__**Diplomacy:**__



__**Royal Family:**__

-

__**Generals:**__

-`})
						)
						.then(channel => country.setDiscordChannelId(channel.id) )
						.then(() => msg.reply("new country created successfully") )
						.catch(err => {
							console.error(err)
							msg.reply("failed to create country database entry, please contact <@!439790095122300928>")
							throw err
						})
				}
			}

			const player = new Player(target.id, country)
			await globalThis.guild.members.fetch(target.id)
				.then(gm => gm.roles.add(config.currentSeasonRole))
			Player.insert(player)
				.then(() => {
					msg.reply("User registered successfully")
					if (country) {
						funcs.getMember(player.discordId).then(() => {
							country.discordChannel
								.permissionOverwrites.create(player.discordId, {VIEW_CHANNEL: true}, {reason: "new player joined country", type: 1})
								.catch(err => console.error(err) || msg.reply("failed to give new player permission over country channel, please contact <@!439790095122300928>"))
						})
					} else {
						msg.reply(`You didn't assign a country to the player, you can do so by calling \`${config.prefix}country set <player> <country>\``)
					}

				})
				.catch(err => console.error(err) || msg.reply("failed to create player database entry, please contact <@!439790095122300928>"))
		},
		Command.perm.admins
	),
	nextturn: new Command(
		(msg, args) => {
			let turnActionCount
			if (args[0]) {
				turnActionCount = parseInt(args[0])
			}
			Country.gets()
				.then(countries => Promise.all(
					countries.map(
						c => c.setAction( turnActionCount ? c.maxAction - turnActionCount : Math.max(c.action - c.maxAction, 0) )
					)
				))
				.then(() => msg.reply("Action count ready for new turn"))
				.catch(err => console.error(err) || msg.reply(`Something unexpected happened, please contact <@!439790095122300928>`))
		},
		Command.perm.admins
	),

	ping: new Command((msg) => msg.reply("pong")),
	poll: new Command(funcs.createPoll),
	random: {
		words: new Command((msg, args) => {
			const answers = args.join` `.split`\n`
			msg.reply(`Chosen word is : **${answers[Math.floor(Math.random() * answers.length)]}**`)
		}),
		default: new Command((msg, args) => {
			let min = 1
			let max = 2
			switch (args.length) {
				case 1: max = ~~args[0]; break
				case 2: min = ~~args[0]; max = ~~args[1]; break
			}
			msg.reply(`Random between **${min}** and **${max}** gave : **${Math.floor(Math.random() * (max - min + 1) + min)}**`)
		})
	},

	finduser: new Command((msg, args) => {
		Promise.all(
			args.map(
				arg => funcs.getMember(arg)
			)
		)
			.then(members => members.map(m => m ? `${m.nickname ?? m.user.username} : ${m.id}` : m))
			.then(result => msg.channel.send(result.filter(v=>v).join`\n`))
	}),

	"-;": new Command((msg, args) => {
		const gifs = [
			"https://tenor.com/view/cute-cat-crying-tears-sad-emotional-gif-15881815",
			"https://cdn.discordapp.com/attachments/362925179703132161/839968082512314398/tenor.png",
			"https://cdn.discordapp.com/attachments/362925179703132161/839968082725306368/images_1.jpeg",
			"https://cdn.discordapp.com/attachments/362925179703132161/839968082881282058/unnamed-1.png"
		]
		msg.channel.send(gifs[Math.floor(Math.random() * gifs.length)])
	}),

	default: new Command((msg, args) => {
		msg.reply(`Command not found : ${args.join` `}`)
	})
}
comm.actions = comm.action
comm.a = comm.action

comm.e = comm.eval
comm.c = comm.country
comm.eco = comm.economy

comm.r = comm.random
comm.random.word = comm.random.words
comm.random.w = comm.random.words
comm.d = comm.random


module.exports = comm

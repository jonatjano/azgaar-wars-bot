const Discord = require("discord.js")
const config = require("./config.js")

const commands = require("./commands/commands.js")
const dbManager = require("./database/dbManager.js")
const emoji = require("./helpers/emoji")
const funcs = require("./helpers/functions")
const Player = require("./database/Player.js")
const Country = require("./database/Country.js")
const {Intents} = require("discord.js");

const IS_DEV = process.env.NODE_ENV !== "production"
let devMode = IS_DEV

/**
 * @type {Discord.Client}
 */
const client = new Discord.Client({intents: [
		Intents.FLAGS.GUILDS,
		Intents.FLAGS.GUILD_MEMBERS,
		Intents.FLAGS.GUILD_BANS,
		Intents.FLAGS.GUILD_EMOJIS_AND_STICKERS,
		Intents.FLAGS.GUILD_INTEGRATIONS,
		Intents.FLAGS.GUILD_WEBHOOKS,
		Intents.FLAGS.GUILD_INVITES,
		Intents.FLAGS.GUILD_VOICE_STATES,
//		Intents.FLAGS.GUILD_PRESENCES,
		Intents.FLAGS.GUILD_MESSAGES,
		Intents.FLAGS.GUILD_MESSAGE_REACTIONS,
		Intents.FLAGS.GUILD_MESSAGE_TYPING,
		Intents.FLAGS.DIRECT_MESSAGES,
		Intents.FLAGS.DIRECT_MESSAGE_REACTIONS,
		Intents.FLAGS.DIRECT_MESSAGE_TYPING
	]
})
globalThis.client = client
globalThis.guild = null;

client.on("ready", () => {
	console.log(`Logged in as ${client.user.tag}!`)
	client.guilds.fetch("809022392512020480").then(
		/**
		 * @param {Discord.Guild} guild
		 */
		g => {
			console.log("found guild " + g.name)
			guild = g
		}
	)
})

client.on("messageCreate", async msg => {

	if (guild === null || msg.guild === null) { return }

	for (let pref in config.prefixes) {
		if (msg.content.startsWith(pref)) {
			msg.content = msg.content.replace(pref, config.prefix + config.prefixes[pref])
		}
	}

	let prefix
	if (msg.content.startsWith("<@!819622185625714698>")) { prefix = "<@!819622185625714698>" } // bot ping
	if (msg.content.startsWith("<@819622185625714698>")) { prefix = "<@819622185625714698>" } // bot ping
	if (msg.content.startsWith(config.prefix)) { prefix = config.prefix }

	if (prefix) {
		msg.content = msg.content.substring(prefix.length)
		msg.content = msg.content.trimStart()
		const [...args] = msg.content.split(/ +/g)

		let command = commands
		let subCommand = args.shift()

		while (subCommand && command[subCommand.toLowerCase()]) {
			command = command[subCommand.toLowerCase()]
			subCommand = args.shift()
		}
		if (command.default) {
			command = command.default
		}
		if (subCommand) {
			args.unshift(subCommand)
		}
		if (!command) {
			return msg.reply("Command not found")
		}

		if (await command.permission.condition(msg.author)) {
			command.call(msg, args)
		} else if (command.permission.feedback) {
			msg.reply(command.permission.feedback)
		}
	}
	else if (msg.content.startsWith("<@&818337840383524874>")) { // poll ping role

		funcs.createPoll(msg)

	}
	else if (msg.author.id === config.ecoBot.botId) {

			if (
				msg.embeds.length === 1 &&

				msg.embeds[0].description &&
				! msg.embeds[0].description.startsWith("<:stopwatch:630927808843218945>") &&
				! msg.embeds[0].description.startsWith("<:xmark:773218895150448640>") &&

				msg.embeds[0].author
			) {
				const [username, discriminator] = msg.embeds[0].author.name.split`#`
				const userPromise = guild.members.fetch({query: username})

				userPromise.then(userList => {

					const user = userList.find(u => u.user.username === username && u.user.discriminator === discriminator)
					if (! user) { return }

					msg.channel.messages.fetch({before: msg.id, limit: 5}).then(messages => {

						const commandMessage = messages.find(m => m.author.id === user.id)
						const command = config.ecoBot.commands.find(comm =>
							commandMessage.content.match(new RegExp("^\\" + config.ecoBot.prefix + comm.regex.source, "i"))
						)

						if (! command) { return }

						const quantity = command.regex.exec(commandMessage.content).groups?.quantity ?? 1
						const cost = command.cost(+quantity)

						const player = new Player(user.id)
						player.init()
							.then(p => (p.country ? p.country.setAction(p.country.action + cost) : msg.reply("Player is homeless, <@818476619906940949> please remove that man money"))
								.then(() => commandMessage.react(emoji.general.validate))
								.catch(err => console.error(err) || commandMessage.reply("failed to update database entry, please contact <@!439790095122300928>"))
							)
							.catch(err => console.error(err) || commandMessage.reply("failed to get user data, please contact <@!439790095122300928>"))

						// msg.channel.send(`<@439790095122300928> ${user.user.username} used the command : "${commandMessage}" costing him ${cost} action${cost > 1 ? "s" : ""}`)
					})
				})
			}
			else if (msg.content.startsWith("You have hired builders to construct")) {

				msg.channel.messages.fetch({before: msg.id, limit: 10}).then(messages => {
					const commandMessage = messages.find(m => m.content.startsWith(`${config.ecoBot.prefix}buy`))
					const command = config.ecoBot.commands.find(comm =>
						commandMessage.content.match(new RegExp("^\\" + config.ecoBot.prefix + comm.regex.source, "i"))
					)

					if (! command) { return }

					const quantity = command.regex.exec(commandMessage.content).groups?.quantity ?? 1
					const cost = command.cost(+quantity)

					const player = new Player(commandMessage.author.id)
					player.init()
						.then(p => (p.country ? p.country.setAction(p.country.action + cost) : msg.reply("Player is homeless, <@818476619906940949> please refund that man money and empty his inventory"))
							.then(() => commandMessage.react(emoji.general.validate))
							.catch(err => console.error(err) || commandMessage.reply("failed to update database entry, please contact <@!439790095122300928>"))
						)
						.catch(err => console.error(err) || commandMessage.reply("failed to get user data, please contact <@!439790095122300928>"))

					// msg.channel.send(`<@439790095122300928> ${user.user.username} used the command : "${commandMessage}" costing him ${cost} action${cost > 1 ? "s" : ""}`)
				})
			}

		}
	}
)

client.on("messageReactionAdd", (reaction, author) => {
	if (config.botAdmins.includes(author.id)) {
		const costMap = {
			[emoji.digit["1"]]: 1,
			[emoji.digit["2"]]: 2,
			[emoji.digit["3"]]: 3,
			[emoji.digit["4"]]: 4,
			[emoji.digit["5"]]: 5,
			[emoji.digit["6"]]: 6,
			[emoji.digit["7"]]: 7,
			[emoji.digit["8"]]: 8,
			[emoji.digit["9"]]: 9,
			[emoji.digit["0"]]: 10
		}
		if (costMap[reaction.emoji.name] === undefined) { return }

		const cost = costMap[reaction.emoji.name]

		const player = new Player(reaction.message.author.id)
		player.init()
			.then(p => (p.country ? p.country.setAction(p.country.action + cost) : msg.reply("Player is homeless"))
				.then(() => reaction.message.react(emoji.general.validate))
				.catch(err => console.error(err) || reaction.message.channel.send(`<@${author.id}> failed to update database entry, please contact <@!439790095122300928>`))
			)
			.catch(err => console.error(err) || reaction.message.channel.send("<@${author.id}> failed to get user data, please contact <@!439790095122300928>"))
	}
})

// client.on("guildMemberAdd", guildMember => {
// 	console.log(`member joined ${guildMember.nickname ?? guildMember.user.username}`)
//
// 	if (guildMember.guild.id !== guild.id) { return }
//
// 	const member = new Member(guildMember.id)
// 	return Member.insert(member)
// 		.then(a => console.log(`member inserted ${guildMember.nickname ?? guildMember.user.username}`))
// 		.catch(err => {
// 			console.error(err)
// 			if (err.errno !== 19) {
// 				guild.channels.resolve("818082039893852191").send(`<@!439790095122300928> I failed to create a database entry for the new user <@${guildMember.id}>`)
// 			}
// 		})
// })

client.login(process.env.BOT_TOKEN);

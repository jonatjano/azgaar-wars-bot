const sqlite3 = require("sqlite3").verbose()

const db = new sqlite3.Database("./db.sqlite")
db.on("open", () => {
	console.log("Database successfully opened")
	db.run("PRAGMA foreign_keys = ON")
})

module.exports = {
	sqlRun(query) {
		return new Promise((res, rej) => {
			db.run(query, undefined, (err) => {
				if (err) {
					return rej(err)
				}
				res("success")
			})
		})
	},
	sqlAll(query) {
		return new Promise((res, rej) => {
			db.all(query, undefined, (err, rows) => {
				if (err) {
					return rej(err)
				}
				res(rows)
			})
		})
	},
	db
}

const dbManager = require("../database/dbManager");
const {db} = dbManager
const config = require("../config.js")

db.run(`create table if not exists country (
	id integer primary key,
	discordChannelId text unique,
	action integer not null,
	maxAction integer not null,
	money integer default 0
)`)

class Country {
	#id
	#discordChannelId
	#action
	#maxAction
	#money

	constructor(id = null, name = "") {
		this.#id = id
		this.#discordChannelId = ""
		this.#action = 0
		this.#maxAction = config.actions.defaultMax
		this.#money = 0
	}

	init(id = null) {
		id = this.#id ?? id
		if (id === null) {throw TypeError("id is null")}
		return get(id)
			.then(row => row ? this.hydrate(row) : undefined)
	}

	updateEndTurnRoles() {
		Country.getPlayerDiscordIds(this).then(playerIds =>
			playerIds.map(({discordId}) =>
				this.action >= this.maxAction ?
					globalThis.guild.members.fetch(discordId)
						.then(gm => gm.roles.add(config.actions.endTurnRole))
					:
					globalThis.guild.members.fetch(discordId)
						.then(gm => gm.roles.remove(config.actions.endTurnRole))
			)
		)
	}

	get id() { return this.#id }

	get discordChannelId() { return this.#discordChannelId }
	set discordChannelId(val) { this.setDiscordChannelId(val) }
	setDiscordChannelId(val) {
		const data = this.toDbParams()
		data.$discordChannelId = val
		return update(data).then(res => {
			this.#discordChannelId = val
			return this
		})
	}
	get discordChannel() { return globalThis.guild.channels.resolve(this.discordChannelId) }

	get action() { return this.#action }
	set action(val) { this.setAction(val) }
	setAction(val) {
		const data = this.toDbParams()
		data.$action = val
		return update(data).then(res => {
			this.#action = val
			this.updateEndTurnRoles()
			return this
		})
	}

	get maxAction() { return this.#maxAction }
	set maxAction(val) { this.setMaxAction(val) }
	setMaxAction(val) {
		const data = this.toDbParams()
		data.$maxAction = val
		return update(data).then(res => {
			this.#maxAction = val
			this.updateEndTurnRoles()
			return this
		})
	}

	get name() { return this.discordChannel.name }

	get money() { return this.#money }
	set money(val) { this.setMoney(val) }
	setMoney(val) {
		const data = this.toDbParams()
		data.$money = val
		return update(data).then(res => {
			this.#money = val
			return this
		})
	}

	hydrate(data) {
		this.#id = data.id
		this.#discordChannelId = data.discordChannelId
		this.#action = data.action
		this.#maxAction = data.maxAction
		this.#money = data.money

		return this
	}

	toDbParams() {
		return {
			$id: this.id,
			$discordChannelId: this.discordChannelId,
			$action: this.action,
			$maxAction: this.maxAction,
			$money: this.money
		}
	}

	toJSON() {
		return {
			id: this.id,
			discordChannelId: this.discordChannelId,
			action: this.action,
			maxAction: this.maxAction,
			money: this.money
		}
	}

	static hydrateNew(data) {
		const c = new Country()
		return c.hydrate(data)
	}

	static get(country) { return get(country) }
	static getPlayerDiscordIds(country) { return getPlayerDiscordIds(country) }
	static getsByName(name) {
		return gets().then(countries => countries.filter(c => c.name.toLowerCase().includes(name.toLowerCase())))
	}
	static gets() { return gets() }
	static insert(country) {
		return insert(country).then(id => {
			if (country instanceof Country) {country.#id = id}
			return country
		})
	}
	static update(country) { return update(country) }
}

function get(country) {
	return new Promise((res, rej) => {
		if (country instanceof Country) { country = country.toDbParams() }
		else if (typeof country === "number" || typeof country === "string") { country = {$id: country} }
		db.get(`select * from country where id = $id`, {$id: country.$id}, (err, row) => {
			if (err) {return rej(err)}
			if (!row) {return res(undefined)}
			res(Country.hydrateNew(row))
		})
	})
}
function getPlayerDiscordIds(country) {
	return new Promise((res, rej) => {
		if (country instanceof Country) { country = country.toDbParams() }
		else if (typeof country === "number" || typeof country === "string") { country = {$id: country} }

		db.all(`select discordId from player where country = $id`, {$id: country.$id}, (err, rows) => {
			if (err) { return rej(err) }
			res(rows)
		})
	})
}
function gets() {
	return new Promise((res, rej) => {
		db.all(`select * from country`, undefined, (err, rows) => {
			if (err) {
				return rej(err)
			}
			res(rows.map(r => Country.hydrateNew(r)))
		})
	})
}
function insert(country) {
	if (country instanceof Country) { country = country.toDbParams() }

	return new Promise((res, rej) => {
		db.run(`insert into country values ($id, $discordChannelId, $action, $maxAction, $money)`, country, function(err, rows) {
			if (err) {
				return rej(err)
			}
			res(this.lastID)
		})
	})
}
function update(country) {
	if (country instanceof Country) { country = country.toDbParams() }

	return new Promise((res, rej) => {
		db.run(`update country set discordChannelId = $discordChannelId, action = $action, maxAction = $maxAction, money = $money where id = $id`, country, (err, rows) => {
			if (err) {
				return rej(err)
			}
			res(`successfully updated ${this.change} row(s)`)
		})
	})
}

module.exports = Country

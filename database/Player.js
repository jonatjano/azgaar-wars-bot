const dbManager = require("../database/dbManager");
const {db} = dbManager
const config = require("../config.js")
const Country = require("./Country.js")

db.run(`create table if not exists player (
	discordId text primary key,
	country integer references country(id) ON DELETE SET NULL
)`)

class Player {
	#discordId
	/**
	 * @type {Country}
	 */
	#country

	constructor(discordId = null, country = null) {
		this.#discordId = discordId
		this.#country = country
	}

	init(discordId = null) {
		discordId = this.#discordId ?? discordId
		if (discordId === null) {throw TypeError("discordId is null")}
		return get(discordId)
			.then(row => row ? this.hydrate(row) : undefined)
	}

	get discordId() { return this.#discordId }

	get country() { return this.#country }
	set country(country) { this.setCountry(country) }
	async setCountry(country) {
		if (! country instanceof Country) {
			country = await Country.get(country)
		}
		const data = this.toDbParams()
		data.$country = country === null ? null : country.id
		return update(data).then(res => {this.#country = country; return this})
	}

	async hydrate(data) {
		this.#discordId = data.discordId
		if (data.country !== null) {
			this.#country = await Country.get(data.country)

			if (this.#country === null) { throw TypeError(`Country with id : ${data.country} not found when hydrating player ${data.discordId}`) }
		}

		return this
	}

	toDbParams() {
		return {
			$discordId: this.discordId,
			$country: this.country === null ? null : this.country.id
		}
	}

	toJSON() {
		return {
			discordId: this.discordId,
			country: this.country === null ? null : this.country.toJSON()
		}
	}

	static hydrateNew(data) {
		const p = new Player()
		return p.hydrate(data)
	}

	static get(player) { return get(player) }
	static getsByCountry(country) { return getsByCountry(country) }
	static gets() { return gets() }
	static insert(player) { return insert(player) }
	static update(player) { return update(player) }
}

function get(player) {
	return new Promise((res, rej) => {
		if (player instanceof Player) { player = player.toDbParams() }
		else if (typeof player === "string") { player = {$discordId: player} }
		db.get(`select * from player where discordId = $discordId`, {$discordId: player.$discordId}, (err, row) => {
			if (err) {return rej(err)}
			if (!row) {return res(undefined)}
			res(Player.hydrateNew(row))
		})
	})
}

function getsByCountry(country) {
	return new Promise((res, rej) => {
		if (country instanceof Country) { country = {$country: country.id} }
		else { country = {$country: country} }

		db.all(`select * from player where country = $country`, country, (err, rows) => {
			if (err) {return rej(err)}
			res(rows.map(r => Player.hydrateNew(r)))
		})
	})
}

function gets() {
	return new Promise((res, rej) => {
		db.all(`select * from player`, undefined, (err, rows) => {
			if (err) {
				return rej(err)
			}
			res(rows.map(r => Player.hydrateNew(r)))
		})
	})
}
function insert(player) {
	if (player instanceof Player) { player = player.toDbParams() }

	return new Promise((res, rej) => {
		db.run(`insert into player values ($discordId, $country)`, player, (err, rows) => {
			if (err) {
				return rej(err)
			}
			res("success")
		})
	})
}
function update(player) {
	if (player instanceof Player) { player = player.toDbParams() }

	return new Promise(async (res, rej) => {
		db.run(`update player set country = $country where discordId = $discordId`, player, (err, rows) => {
			if (err) {
				return rej(err)
			}
			res(`successfully updated ${this.change} row(s)`)
		})
	})
}

module.exports = Player

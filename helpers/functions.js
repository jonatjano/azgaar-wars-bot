const Country = require("../database/Country");
const {exec} = require("child_process")

module.exports = {
	exec: function(command) {
		return new Promise((res, rej)  => {
			exec(command, (error, stdout, stderr) => {
				if (error) {
					return rej({error, stderr})
				}
				res(stdout)
			})
		})
	},

	createPoll(msg) {
		const emotes = [...msg.content.matchAll(/<:\w+:\d+>/g)]
			.map(match => match[0]) // <:name:id>
			.map(match => match.split`:`[2].replace(">", "")) // id

		emotes.forEach(emote => {
			msg.react(msg.guild.emojis.cache.get(emote))
		})
	},

	toUserId(pingOrId) {
		return (/^<@!?(\d+)>$/g.exec(pingOrId) ?? /^(\d+)$/g.exec(pingOrId) ?? [])[1]
	},

	/**
	 * @param arg
	 * @return {Promise<GuildMember | null>}
	 */
	getMember(arg) {
		if (! arg) {return new Promise(r => r(null));}

		const testGetId = (/^<@!?(\d+)>$/g.exec(arg) ?? /^(\d+)$/g.exec(arg) ?? [])[1]
		if (testGetId) {
			return guild.members.fetch(testGetId)
				.then(res => res ?? guild.members.fetch({query :testGetId, limit: 1}).then(m => m.values().next().value))
		} else {
			return guild.members.fetch({
				query: (arr=>arr.slice(0,Math.max(1, arr.length-1)).join``)(arg.split`#`),
				limit: 1
			}).then(m => m.values().next().value)
		}
	},

	sendMessageToCountryChannelIfEmpty(country) {
		Country.getPlayerDiscordIds(country)
			.then(players => {
				if (players.length === 0) {
					country.discordChannel.send("There is no player left in that country.")
				}
			})
	}
}


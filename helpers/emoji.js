let servSpec = undefined

module.exports = {
	general: { validate: "✅"},
	digit: { 1: "1️⃣", 2: "2️⃣", 3: "3️⃣", 4: "4️⃣", 5: "5️⃣", 6: "6️⃣", 7: "7️⃣", 8: "8️⃣", 9: "9️⃣", 0: "0️⃣" },
	get serverSpecific() {
		if (servSpec) {
			return servSpec
		}
		if (guild) {
			servSpec = {}
			guild.emojis.cache.each(a => {
				Object.defineProperty(servSpec, a.name, {
					value: guild.emojis.resolve(a.id)
				})
			})
		}
		return servSpec
	}
}

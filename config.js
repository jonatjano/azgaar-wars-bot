module.exports = {
	prefix: process.env.NODE_ENV === "production" ? ";" : "-",
	prefixes: process.env.NODE_ENV === "production" ? {
		"$": "economy "
	} : {},
	botAdmins: [
		"439790095122300928", // Jonatjano
		"445655817312665601", // Pie
		"548498897366941697", // York
		"728751603510804550", // Zogar
	],
	currentSeasonRole: "881248157579759640",
	channels: {
		categories: {
			country: process.env.NODE_ENV === "production" ? "818192472064131072" : "879677960213381170"
		}
	},
	actions: {
		defaultMax: 5,
		endTurnRole: "818854746105970759"
	},
	ecoBot: {
		botId: "292953664492929025",
		prefix: "$",
		commands: [
			{regex: /work/i, cost: () => 1},
			{regex: /crime/i, cost: () => 1},
			{regex: /(?:item-)?buy (?<quantity>\d+)?.+/i, cost: qty => Math.ceil(qty / 3)},
		]
	},
}
